#!/usr/bin/env python
import os, shutil, sys, glob
import os.path as path

help_message = '''
backup.py is a simple script to back up any files that campers might modify.
To use it first run 'python backup.py' to create backups of all relevant scripts.
Then to revert a single file to its previous state, use 'python backup.py FILENAME'.
To revert everything, use 'python backup.py nuke'.
'''

def backup():
    files = glob.glob('*.py')
    if path.exists('.backups'):
        shutil.rmtree('.backups')
    os.mkdir('.backups')
    for f in files:
        shutil.copy(f, '.backups')


def revert(f):
    backup = path.join('.backups', f)
    if not path.exists(backup):
        print "No backup for %s" % f
        exit(1)
    shutil.copy(backup, f)

def revert_all():
    for f in glob.glob('*.py'):
        revert(f)


if len(sys.argv) == 1:
    backup()
elif sys.argv[1] == 'nuke':
    revert_all()
elif sys.argv[1] == 'help':
    print help_message
else:
    for f in sys.argv[1:]:
        revert(f)

