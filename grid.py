from works.mc import *

# A large grid
big_grid = [ [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 0, 1, 0, 1, 1, 1, 0],
        [0, 1, 1, 1, 0, 0, 1, 0, 0],
        [0, 1, 0, 1, 0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0] ]


#The default grid prints an 'H' 
small_grid = [ [ 1, 0, 0, 1],
     [ 1, 0, 0, 1],
     [ 1, 1, 1, 1],
     [ 1, 0, 0, 1],
     [ 1, 0, 0, 1] ]

# create a global minecraft instance
mc = connect()   

setGrid(mc, small_grid, STONE, GLASS)
