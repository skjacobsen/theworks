from works.mc import *
mc = connect()

def sphere(size, block):
    middle = makePos(size, size, size)
    pos    = getPos(mc)
    stepForward(pos, 10)
    
    for x, y, z in allPoints(size * 2, size * 2, size * 2):
        p = makePos(x, y, z)
        if distance(middle, p) <= size:
            newPos = offsetPoint(pos, x, y, z)
            set(mc, newPos, block)

sphere(5, STONE)
