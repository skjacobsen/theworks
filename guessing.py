def get_number():
    return int(raw_input())

secret_number = 7
guess         = 0

print "Guess a number!"
while guess != secret_number:
    guess = get_number()
    if guess == secret_number:
        print "You got it!"
    else:
        print "Guess again"
