import minecraft
import block
from math import *

colors = [14, 1, 4, 5, 3, 11, 10]

mc = minecraft.Minecraft.create()
height = 60

pos = mc.player.getPos()

mc.setBlocks(pos.x-64,pos.y + 1, pos.z + 1,pos.x + 64, pos.y + height + len(colors),pos.z + 1,0)
for x in range(0, 128):
    for colorindex in range (0, len(colors)):
        y = sin((x / 128.0) * pi) * height + colorindex
        mc.setBlock(pos.x + x, pos.y + int(y), pos.z+1, block.WOOL.id, colors[len(colors) - 1 -colorindex])
        mc.setBlock(pos.x + x, pos.y + int(y)-10, pos.z+1, block.WOOL.id, colors[len(colors) - 1 -colorindex])
        mc.setBlock(pos.x + x, pos.y + int(y)+20, pos.z+1, block.WOOL.id, colors[len(colors) - 1 -colorindex])
