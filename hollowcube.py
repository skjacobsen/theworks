from works.mc import *

mc = connect()

def isEdge(x, y, z, size):
    if x == 0 or y == 0 or z == 0:
        return True
    elif x == size - 1 or y == size - 1 or z == size - 1:
        return True
    else:
        return False
    

def hollowCube(size, outerBlock, innerBlock):
    pos = getPos(mc)
    stepForward(pos, 10)
    
    for x, y, z in allPoints(size, size, size):
        blockType = innerBlock
        if isEdge(x, y, z, size):
            blockType = outerBlock
        blockPos = offsetPoint(pos, x, y, z)
        set(mc, blockPos, blockType)
        
        
hollowCube(5, STONE, LAVA)
