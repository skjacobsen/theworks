from works.mc import *

mc = connect()

pos = getPos(mc)
stepForward(pos, 10)

size = 10
type = GLASS

setCube(mc, pos, size, type)
