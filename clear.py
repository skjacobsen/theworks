#! /usr/bin/python
from works.mc import *
import minecraft as minecraft
import block as block
mc = connect()

x,y,z=getPos(mc)
""" clearZone clears an area and sets a stone floor
    takes two x,z pairs clears everything above 0y and then sets
    a stone floor at -1y
    @author: goldfish"""

def clearZone( alocx, alocz, blocx, blocz ):
    mc.setBlocks( x+alocx, y, z+alocz, x+blocx, y+128, z+blocz, block.AIR )
    mc.setBlocks( x+alocx, y-1, z+alocz, x+blocx, y-1, z+blocz, block.STONE )

mc = minecraft.Minecraft.create( )
clearZone( -10, -10, 10, 10 )

