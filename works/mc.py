import minecraft
import time
from math    import *
from block   import *
from letters import *

def connect():
    '''
    Connect to the minecraft game
    '''
    return minecraft.Minecraft.create()

def set(mc, pos, block_type):
    '''
    Set a block to block_type at pos
    '''
    mc.setBlock(pos.x, pos.y, pos.z, block_type)
    
def getPos(mc):
    return mc.player.getPos()

def stepForward(pos, n):
    pos.x += n
def stepBack(pos, n):
    pos.x -= n
def stepRight(pos, n):
    pos.z += n
def stepLeft(pos, n):
    pos.z -= n
def stepUp(pos, n):
    pos.y += n
def stepDown(pos, n):
    pos.y -= n

class Point(object):
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

def makePos(x, y, z):
    '''
    Return a position corresponding to the supplied coordinates
    '''
    return Point(x, y, z)

def setGrid(mc, grid, type, blank):
    pos = getPos(mc)
    stepForward(pos, len(grid))
    for x, y, _ in allPoints(len(grid[0]), len(grid), 1):
            if grid[y][x] == 1:
                set(mc, offsetPoint(pos, 0, y, x), type)
            else:
                set(mc, offsetPoint(pos, 0, y, x), blank)

def setCube(mc, pos, size, type):
    mc.setBlocks(pos.x, pos.y, pos.z, pos.x + size, pos.y + size, pos.z + size, type)

def offsetPoint(pos, x, y, z):
    newPos = makePos(pos.x, pos.y, pos.z)
    stepLeft(newPos, z)
    stepUp(newPos, y)
    stepForward(newPos, x)
    return newPos

def allPoints(x, y, z):
    for _x in range(0, x):
        for _y in range(0, y):
            for _z in range(0, z):
                yield (_x, _y, _z)

def setBlocks(mc, p1, p2, type):
    mc.setBlocks(mc, p1.x, p1.y, p1.z, p2.x, p2.y, p2.z, type)

def distance(p1, p2):
    x = pow(p1.x - p2.x, 2)
    y = pow(p1.y - p2.y, 2)
    z = pow(p1.z - p2.z, 2)
    return floor(sqrt(x + y + z))

def postToChat(mc, message):
    mc.postToChat(message)

def sleep(n):
    time.sleep(n)
