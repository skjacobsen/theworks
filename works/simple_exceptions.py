exception_banner = "Uhoh! Somethings gone wrong!"

class OhUh (Exception):
    '''
    UhOh is a human readable exception. It comes with a
    simple explanation for what went wrong as well as a
    more detailed original exception.
    '''
    def __init__(self, original, help_text):
        self.help_text = help_text
        self.original  = original
    def __str__(self):
        for_humans = exception_banner + "\n\tWhat Happened?\n" ++ self.help_text
        precise_error = "\n\tThe Gory Details:" + str(self.original)
        return for_humans + precise_error

