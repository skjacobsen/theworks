'''
    Tower creator! This program makes a tower next to the player. Then, the player can be
    teleported to the top of the tower, or flown to the top of the tower
'''

import minecraft
import block
import time

mc = minecraft.Minecraft.create()

def createTower(number, type):
    pos = mc.player.getPos()
    mc.setBlocks(pos.x+1, pos.y, pos.z+1, pos.x+1, pos.y+number, pos.z+1, type)

    pos.y = pos.y+number
    pos.x = pos.x+1
    pos.z = pos.z+1
    return pos
'''
    This method flys the character to the top of the given position
      pos holds x, y, and z coordinates

    Explanation:
      First, we compute the height difference between the player the the provided position
      Second, we enter the loops
        The loops move the player one block, place a glass block under the player, and
        deletes the block two spaces away from the player. This simulates flying
        There's a loop for x, y, and z coordinate movement
      Finally, we get rid of the last block we placed, and then move the player over to the x
      and z coordinates. This should teleport the player to the top of the location, even if
      something got messed up during the 'flying' movement
'''
def flyTo(pos):
    player = mc.player.getPos()
    difference = pos.y - player.y + 1
    print(difference)
    
    i = 0
    while i < difference:
        mc.player.setPos(player.x, player.y+1, player.z)
        mc.setBlock(player.x, player.y, player.z, block.GLASS)
        mc.setBlock(player.x, player.y-1, player.z, block.AIR)
 
        player = mc.player.getPos()
        time.sleep(0.05)
        i = i + 1
    mc.setBlock(player.x, player.y-1, player.z, block.AIR)

    difference = pos.x - player.x
    i = 0
    while i < difference:
        mc.player.setPos(player.x+1, player.y, player.z)
        mc.setBlock(player.x+1, player.y-1, player.z, block.GLASS)
        mc.setBlock(player.x-1, player.y-1, player.z, block.AIR)

        player = mc.player.getPos()
        time.sleep(0.05)
        i = i + 1
    mc.setBlock(player.x-1, player.y-1, player.z, block.AIR)

    difference = pos.z - player.z
    i = 0
    while i < difference:
        mc.player.setPos(player.x, player.y, player.z+1)
        mc.setBlock(player.x, player.y-1, player.z+1, block.GLASS)
        mc.setBlock(player.x, player.y-1, player.z-1, block.AIR)
        
        player = mc.player.getPos()
        time.sleep(0.05)
        i = i + 1
    mc.setBlock(player.x, player.y-1, player.z-1, block.AIR)

    mc.player.setPos(pos.x, player.y+1, pos.z)

'''
    This method teleports the player to the top of the provided position
      pos holds x, y, and z coordinates
'''
def teleportTo(pos):
    mc.player.setPos(pos.x, pos.y+1, pos.z)
    mc.postToChat("Teleported!")

number = 10
type = block.ICE
tower = createTower(number, type)
mc.postToChat("Whoa! Can you find the tower?")
time.sleep(5)
mc.postToChat("Let's fly to the top of the tower!")
flyTo(tower)
