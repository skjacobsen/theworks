import minecraft
import block
import time

mc = minecraft.Minecraft.create()

blocks_placed  = 0
prev_pos = mc.player.getTilePos()

while blocks_placed < 200:
    curr_pos = mc.player.getTilePos()
    if curr_pos != prev_pos:
        mc.setBlock(curr_pos.x, curr_pos.y - 1, curr_pos.z, block.STONE)
        blocks_placed += 1
    prev_pos = curr_pos
    time.sleep(.2)
